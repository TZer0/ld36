# Installation


Set up OpenFrameworks for your system, see: http://openframeworks.cc/download/ 
Put this folder in a subfolder under apps, such as YourOpenFrameworksFolder/apps/myapps/videowaves and compile it with your system-specific compiler. Then clone the [following project](https://github.com/TZer0/ofxBox2d) into addons (contains one fix for that project, may contain more soon).

Clone this project into apps/myApps/ and run make -j4 inside the folder.

If you are on Windows, you can find a package ready for download right [here](http://underhound.eu/tzer0/public_www/Ludum%20Dare/Built%20To%20Last%20%28LD%2036%29.zip) from my [website](http://underhound.eu).
Requires the following [package](https://www.microsoft.com/en-us/download/details.aspx?id=48145) (choose x86!).
