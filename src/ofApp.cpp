#include "ofApp.h"
#include<stdlib.h>
#include<iostream>
#include<iomanip>
#include<math.h>

#define STRUCTDENSE 6.0
#define STRUCTBOUNCE 0.1
#define STRUCTFRIC 4.0
#define WINDDENSE 2.0
#define WINDBOUNCE 1.0
#define WINDFRIC 0.2
#define WINDPART 50

#define SNOWDENSE 10.0
#define SNOWBOUNCE 0.0
#define SNOWFRIC 0.8
#define RAINDENSE 15.0
#define RAINBOUNCE 0.0
#define RAINFRIC 0.8

#define BUILDRADIUS 200
#define NUMLEVELS 7

#define LIMIT(v, l, m) std::min(m, std::max(l, v))


//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowTitle("Built To Last (The Future is The Past)");
	m_world.init();
	m_world.setGravity(0, 10);
	m_world.createGround();
	m_world.createBounds();
	m_world.setFPS(60);
	m_targetLevel = 0;
	m_windRate = m_rainRate = m_snowRate = -1;
	m_simulating = false;
	m_playMusic = true;
	loadLevel(1);
	m_dir.open("music");
	m_tracks = m_dir.listDir();
	m_world.getWorld()->SetContactListener(this);
	m_currTrack = 0;
	if (m_playMusic) {
		setupMusic(false);
	}
}

//--------------------------------------------------------------
void ofApp::update(){
	m_world.update();
	m_world.setGravity(0, 10);
	m_time += 1/60.f;
	if (!m_player.isPlaying() && m_playMusic) {
		setupMusic();
	}
	if (m_eventTime > 0) {
		m_eventTime -= ofGetLastFrameTime();
	}
	if (m_simulating) {
		m_eventTime = -1;
		if (m_time > m_targetTime) {
			m_eventSuccess = validateState();
			m_eventTime = 6;
			toggleSim();
			if (m_eventSuccess) {
				m_beaten.insert(m_level);
				loadLevel(m_level+1);
			}
			return;
		}
		if (m_windRate > 0) {
			setupWind();
		}
		if (m_snowRate > 0) {
			setupSnow();
		}
		if (m_rainRate > 0) {
			setupRain();
		}
	}
	cleanup();
}

bool ofApp::validateState()
{
	for (size_t i=0; i < m_structure.size(); i++) {
		b2Body *body = m_structure[i].get()->body;
		const b2Transform& trans = body->GetTransform();
		for (b2Fixture *fixture = body->GetFixtureList(); fixture != NULL; fixture = fixture->GetNext()) {
			b2Shape *shape = fixture->GetShape();
			switch (shape->GetType()) {
			case b2Shape::Type::e_circle:
			{
				b2CircleShape *c_shape = (b2CircleShape*) shape;
				int32 count = c_shape->GetVertexCount();
				for (int32 j = 0; j < count; j++) {
					b2Vec2 v = c_shape->GetVertex(j);
					auto pt = worldPtToscreenPt(b2Mul(trans, v));
					pt.y -= c_shape->m_radius*OFX_BOX2D_SCALE;
					if (validatePoint(pt)) {
						return true;
					}
				}
			}
				break;
			case b2Shape::Type::e_polygon:
			{
				b2PolygonShape *c_shape = (b2PolygonShape*) shape;
				int32 count = c_shape->GetVertexCount();
				for (int32 j = 0; j < count; j++) {
					b2Vec2 v = c_shape->GetVertex(j);
					ofPoint pt = worldPtToscreenPt(b2Mul(trans, v));
					if (validatePoint(pt)) {
						return true;
					}
				}
			}
				break;
			default:
				break;
			}

		}
	}
	return false;
}

bool ofApp::validatePoint(const ofPoint &pt)
{
	return pt.y < 600-m_targetLevel && pt.x <= 1024/2+BUILDRADIUS && pt.x >= 1024/2-BUILDRADIUS;
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofFill();
	ofSetHexColor(0xe63b8b);
	for (size_t i=0; i < m_structure.size(); i++) {
		m_structure[i].get()->draw();
	}
	ofSetHexColor(0x333333);
	for (size_t i=0; i < m_levelStructure.size(); i++) {
		m_levelStructure[i].get()->draw();
	}
	ofSetHexColor(0x444342);
	for (size_t i=0; i < m_joints.size(); i++) {
		m_joints[i].get()->draw();
	}
	ofSetHexColor(0xaaaaaa);
	for (size_t i=0; i < m_windParticles.size(); i++) {
		m_windParticles[i].get()->draw();
	}
	ofSetHexColor(0xffffff);
	for (size_t i=0; i < m_snowParticles.size(); i++) {
		m_snowParticles[i].get()->draw();
	}
	ofSetHexColor(0x2020ee);
	for (size_t i=0; i < m_rainParticles.size(); i++) {
		m_rainParticles[i].get()->draw();
	}
	ofSetHexColor(0xeeeeee);
	for (size_t i=0; i < m_sleetParticles.size(); i++) {
		m_sleetParticles[i].get()->draw();
	}
	ofSetHexColor(0x000000);
	ofDrawBitmapString(m_message + (m_playMusic ? "\n\nMusic: " + m_trackName + " by TZer0" : ""), 30, 30);
	ofDrawBitmapString(getSimTimeMessage(), 30, 250);
	if (m_eventTime > 0) {
		ofDrawBitmapString(m_eventSuccess ? "Success, welcome to the next level!" : "Failure - try again.", 1024/2, (600/2)-120);
	}
	ofSetHexColor(0xbb0000);
	ofDrawLine(0,600-m_targetLevel, 1024, 600-m_targetLevel);
	ofSetHexColor(0x00bb00);
	ofDrawLine(1024/2-BUILDRADIUS,0, 1024/2-BUILDRADIUS, 600);
	ofDrawLine(1024/2+BUILDRADIUS,0, 1024/2+BUILDRADIUS, 600);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == ' ') {
		toggleSim();
	}
	if (!m_simulating) {
		if (key == 'r' || key == 'R') {
			loadLevel(m_level);
		}
		if (key == 'n' || key == 'N') {
			loadLevel(m_level+1);
		}
		if (key == 'b' || key == 'B') {
			loadLevel(m_level-1);
		}
	}
	if (key == 'm' || key == 'M') {
		m_playMusic = !m_playMusic;
		if (!m_playMusic) {
			m_player.stop();
		} else {
			setupMusic(false);
		}
	}
	if (key == '.' && m_playMusic) {
		setupMusic();
	}
}

void ofApp::toggleSim()
{
	mouseReleased(0,0,0);
	m_simulating = !m_simulating;
	m_windParticles.clear();
	m_snowParticles.clear();
	m_rainParticles.clear();
	m_sleetParticles.clear();
	m_time = 0.0f;
	if (m_simulating) {
		m_save.clear();
		for (size_t i=0; i < m_structure.size(); i++) {
			m_structure[i].get()->setVelocity(0.f,0.f);
			m_save.push_back(SaveState(m_structure[i].get()->body->GetAngle(), m_structure[i].get()->body->GetPosition()));
		}
	} else {
		for (size_t i=0; i < m_structure.size(); i++) {
			if (i >= m_save.size()) {
				ofLog() << "SAVE ERROR";
				break;
			}
			m_structure[i].get()->body->SetTransform(m_save[i].m_pos, m_save[i].m_rot);
			m_structure[i].get()->setVelocity(0.f,0.f);
			m_structure[i].get()->body->SetAngularVelocity(0.f);
		}
	}

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	if (button == 0) {
		m_world.grabShapeDragged(x, y);
	}
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	if (m_simulating) {
		return;
	}
	if (button == 0) {
		m_world.grabShapeDown(x, y);
		if (m_world.mouseJoint != NULL) {
			BODYTYPE at = (BODYTYPE) reinterpret_cast<int>(m_world.mouseJoint->GetBodyB()->GetUserData());
			if (at != HOUSE) {
				mouseReleased(x, y, button);
				return;
			}
			m_world.mouseJoint->GetBodyB()->SetFixedRotation(true);
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	if (button == 0) {
		if (m_world.mouseJoint != NULL) {
			m_world.mouseJoint->GetBodyB()->SetFixedRotation(false);
		}
		m_world.grabShapeUp(x, y);
	}
}

void ofApp::mouseScrolled(int x, int y, float sx, float sy)
{
	if (sy == 0) {
		return;
	}
	if (m_world.mouseJoint != NULL) {
		auto *body = m_world.mouseJoint->GetBodyB();
		body->SetTransform(body->GetWorldCenter(), body->GetAngle() + LIMIT(sy, -1.0f, 1.0f)/10.f);
	}
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}

void ofApp::generateMessage(std::string name)
{
	std::stringstream height;
	height << std::fixed <<  std::setprecision(3) << m_targetLevel/25.;
	m_message = "Level " + std::to_string(m_level) + (m_beaten.find(m_level) != m_beaten.end() ? " - beaten!" : "");
	m_message += "\nYou must " + name + " at least " + height.str() + " m tall between the green lines.";
	if (m_rainRate > 10) {
		m_message += "\nIt will be pouring down.";
	} else if (m_rainRate > 5) {
		m_message += "\nIt will be raining.";
	} else if (m_rainRate > 0) {
		m_message += "\nThere will be some rain.";
	}
	if (m_snowRate > 10) {
		m_message += "\nIt will really hard.";
	} else if (m_snowRate > 5) {
		m_message += "\nIt will be snowing.";
	} else if (m_snowRate > 0) {
		m_message += "\nExpect some snow.";
	}
	if (m_snowRate > 0 && m_rainRate > 0) {
		m_message += "\nThe rain and snow will also combine into sleet (which is heavier!).";
	}
	if (m_windRate > 30) {
		m_message += "\nThere will be a hurricane.";
	} else if (m_windRate > 15) {
		m_message += "\nIt is quite windy.";
	} else if (m_windRate > 0) {
		m_message += "\nIt is slightly windy.";
	}
}

std::string ofApp::getSimTimeMessage()
{
	std::stringstream ret;
	if (m_simulating) {
		ret << std::fixed << "Simulating: " << std::setprecision(1) << m_time << " s/";
		ret << std::fixed << std::setprecision(1) << m_targetTime << " s";
		ret << "\nPress <space> to abort.";
	} else {
		ret << "Press <space> to start simulating.\nPress <r> to restart.\n<lmb> to grab.\n<mouse wheel> to rotate.";
	}
	ret << "\n\nIf you're stuck, press <n> and <b> to change levels.";
	ret << "\nPress <m> to toggle music.\nPress <.> to skip to the next track.";
	return ret.str();
}

void ofApp::setCommonParams(float wind, float rain, float snow, float targetLevel, float targetTime, std::string name) {
	m_rainRate = rain;
	m_snowRate = snow;
	m_windRate = wind;
	m_targetLevel = targetLevel;
	m_targetTime = targetTime;
	generateMessage(name);
}

ofApp::~ofApp() {
	m_structure.clear();
	m_windParticles.clear();
}

void ofApp::loadLevel(int level) {
	mouseReleased(0,0,0);
	m_level = level;
	m_simulating = false;
	m_structure.clear();
	m_levelStructure.clear();
	m_joints.clear();
	if (m_level == NUMLEVELS+1) {
		m_level = 1;
	}
	m_level %= NUMLEVELS+1;
	if (m_level <= 0) {
		m_level = NUMLEVELS;
	}

	if (m_level == 1) {
		setCommonParams(16, 4, 6, 50, 20.0, "construct a house which is");
		auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
		setupBuildBlock(box);
		box.get()->setup(m_world.getWorld(), 100, 100, 30, 30);
		auto triangle = shared_ptr<ofxBox2dPolygon>(new ofxBox2dPolygon);
		setupBuildBlock(triangle);
		triangle.get()->addTriangle(ofVec2f(20,15),ofVec2f(-20,15),ofVec2f(0,-15));
		triangle.get()->create(m_world.getWorld());
		triangle.get()->setPosition(200,200);
	} else if (m_level == 2) {
		setCommonParams(30, -1, -1, 75, 20.0, "construct tower to guide travellers which is");
		for (int i = 0; i < 7; i++) {
			auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
			setupBuildBlock(box);
			box.get()->setup(m_world.getWorld(), 100+i*30, 550, 20, 30);
		}
		for (int i = 0; i < 2; i++) {
			auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
			setupBuildBlock(box);
			box.get()->setup(m_world.getWorld(), 800, 550+i*10, 60, 4);
		}
		auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
		setupStaticBlock(box);
		box.get()->setup(m_world.getWorld(), 600, 600-15, 30, 30);
	} else if (m_level == 3) {
		setCommonParams(30, 10, 10, 300, 20.0, "build a structure to help keep track of season-changes, day-lengths and confuse archaeologists in about 5000 years\nwhich is");
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 3; j++) {
				auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
				setupBuildBlock(box);
				box.get()->setup(m_world.getWorld(), 100+i*35, 430 + j * 70, 30, 70);
			}
		}
	} else if (m_level == 4) {
		setCommonParams(30, 10, 10, 100, 20.0, "to stack some hay bales in a stack which is");
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 2; j++) {
				auto bale = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
				setupBuildBlock(bale);
				bale.get()->setup(m_world.getWorld(), 20+i*40+j*700, 550, 20);
			}
		}
		for (int i = 0; i < 6; i++) {
			auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
			setupBuildBlock(box);
			box.get()->setup(m_world.getWorld(), 500+i*30, 550, 20, 30);
		}
	} else if (m_level == 5) {
		setCommonParams(30, 10, 10, 300, 20.0, "build an imposing defensive structure to scare off foes\nwhich is");
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 3; j++) {
				auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
				setupBuildBlock(box);
				box.get()->setup(m_world.getWorld(), 100+i*30, 450 + j * 30, 20, 30);
			}
		}
		for (int i = 0; i < 15; i++) {
			auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
			setupBuildBlock(box);
			box.get()->setup(m_world.getWorld(), 800, 450+i*10, 80, 4);
		}
	} else if (m_level == 6) {
		setCommonParams(30, -1, -1, 150, 20.0, "to stack some stones on top of that curious floating rock\n(it'll look cool - trust me!) in a structure which is");
		auto rock = shared_ptr<ofxBox2dPolygon>(new ofxBox2dPolygon);
		setupStaticBlock(rock);
		vector<ofVec2f> rockVert = {ofVec2f(-40,-15),ofVec2f(0,-25),ofVec2f(40,-15),ofVec2f(-15,15),ofVec2f(0,20),ofVec2f(15,15)};
		rock.get()->addVertexes(rockVert);
		rock.get()->create(m_world.getWorld());
		rock.get()->setPosition(512, 600-50);
		auto stabiliser1 = shared_ptr<ofxBox2dPolygon>(new ofxBox2dPolygon);
		setupBuildBlock(stabiliser1);
		stabiliser1.get()->addTriangle(ofVec2f(-20,5),ofVec2f(-20,-5),ofVec2f(20,5));
		stabiliser1.get()->create(m_world.getWorld());
		stabiliser1.get()->setPosition(300,600/2);
		auto stabiliser2 = shared_ptr<ofxBox2dPolygon>(new ofxBox2dPolygon);
		setupBuildBlock(stabiliser2);
		stabiliser2.get()->addTriangle(ofVec2f(20,5),ofVec2f(20,-5),ofVec2f(-20,5));
		stabiliser2.get()->create(m_world.getWorld());
		stabiliser2.get()->setPosition(360,600/2);
		setupBuildBlock(stabiliser2);
		auto joint = shared_ptr<ofxBox2dJoint>(new ofxBox2dJoint);
		joint.get()->setup(m_world.getWorld(), stabiliser1.get()->body, stabiliser2.get()->body, 4.f, 10, false);
		joint.get()->setLength(50);
		m_joints.push_back(joint);
		for (int i = 0; i < 7; i++) {
			auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
			setupBuildBlock(box);
			box.get()->setup(m_world.getWorld(), 100+i*30, 550, 20, 30);
		}
	} else if (m_level == 7) {
		setCommonParams(50, 20, -1, 350, 20.0, "create a light-house which is");
		auto box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
		setupStaticBlock(box);
		box.get()->setup(m_world.getWorld(), 500, 600-75, 150, 150);
		box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
		setupStaticBlock(box);
		box.get()->setup(m_world.getWorld(), 408, 600-10, 20, 100);
		box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
		setupStaticBlock(box);
		box.get()->setup(m_world.getWorld(), 500, 600-170, 30, 40);

		box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
		setupBuildBlock(box);
		box.get()->setup(m_world.getWorld(), 800, 600, 250, 4);
		for (int i = 0; i < 5; i++) {
			box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
			setupBuildBlock(box);
			box.get()->setup(m_world.getWorld(), 100+i*30, 450, 20, 30);
		}
		box = shared_ptr<ofxBox2dRect>(new ofxBox2dRect);
		setupBuildBlock(box);
		box.get()->setup(m_world.getWorld(), 70, 450, 20, 210);
	}
	setupUserData();
}

void ofApp::setupUserData()
{
	for (size_t i=0; i < m_structure.size(); i++) {
		m_structure[i].get()->setData((void*)(HOUSE));
	}
	for (size_t i=0; i < m_levelStructure.size(); i++) {
		m_levelStructure[i].get()->setData((void*)(STATIC));
	}
}

void ofApp::setupWind() {
	if (std::floor(m_time*4 - 4/60.) == std::floor(m_time*4)) {
		return;
	}
	int offset = std::fmod(std::floor(m_time*4), 5.f) * (600.f/WINDPART) / 5.f;
	for (int i = 0; i < WINDPART; i++) {
		auto wind = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
		wind.get()->setPhysics(WINDDENSE, WINDBOUNCE, WINDFRIC);
		wind.get()->setup(m_world.getWorld(), 10, i*600./WINDPART + offset, 3);
		wind.get()->body->SetGravityScale(0.0);
		wind.get()->addForce(ofVec2f(m_windRate, 0), WINDDENSE);
		wind.get()->setData((void*)(WIND));
		m_windParticles.push_back(wind);
	}
}

void ofApp::setupSnow() {
	if (std::floor(m_time - 1/60.) == std::floor(m_time)) {
		return;
	}
	int snowFlakes = m_snowRate*10;
	int offset = std::fmod(std::floor(m_time), 5.f) * (1024.f/snowFlakes) / 5.f;
	for (int i = 0; i < snowFlakes; i++) {
		auto snow = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
		snow.get()->setPhysics(SNOWDENSE, SNOWBOUNCE, SNOWFRIC);
		snow.get()->setup(m_world.getWorld(), i*1024.f/snowFlakes + offset, 200, 3);
		snow.get()->body->SetGravityScale(0.1);
		snow.get()->setData((void*)(SNOW));
		m_snowParticles.push_back(snow);
	}
}

void ofApp::setupRain() {
	if (std::floor(m_time - 1/60.) == std::floor(m_time)) {
		return;
	}
	int rainDrops = m_rainRate*10;
	int offset = std::fmod(std::floor(m_time), 5.f) * (1024.f/rainDrops) / 5.f;
	for (int i = 0; i < rainDrops; i++) {
		auto drop = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
		drop.get()->setPhysics(RAINDENSE, RAINBOUNCE, RAINFRIC);
		drop.get()->setup(m_world.getWorld(), i*1024.f/rainDrops + offset, 200, 3);
		drop.get()->body->SetGravityScale(1.0);
		drop.get()->setData((void*)(RAIN));
		m_rainParticles.push_back(drop);
	}
}


void ofApp::setupBuildBlock(shared_ptr<ofxBox2dBaseShape> shape) {
	shape.get()->setPhysics(STRUCTDENSE, STRUCTBOUNCE, STRUCTFRIC);
	m_structure.push_back(shape);
}

void ofApp::setupStaticBlock(shared_ptr<ofxBox2dBaseShape> shape) {
	shape.get()->setPhysics(0.0f, STRUCTBOUNCE, STRUCTFRIC);
	m_levelStructure.push_back(shape);
}

void ofApp::cleanup()
{
	for (b2Body *b : m_toDestroy) {
		bool found = true;
		for (auto itr = m_windParticles.begin(); itr != m_windParticles.end(); itr++) {
			if (&*itr->get()->body == b) {
				m_windParticles.erase(itr);
				break;
			}
		}
		for (auto itr = m_rainParticles.begin(); itr != m_rainParticles.end(); itr++) {
			if (&*itr->get()->body == b) {
				m_rainParticles.erase(itr);
				break;
			}
		}
		for (auto itr = m_snowParticles.begin(); itr != m_snowParticles.end(); itr++) {
			if (&*itr->get()->body == b) {
				m_snowParticles.erase(itr);
				break;
			}
		}
		for (auto itr = m_sleetParticles.begin(); itr != m_sleetParticles.end(); itr++) {
			if (&*itr->get()->body == b) {
				m_sleetParticles.erase(itr);
				break;
			}
		}
		if (found) {
			continue;
		}
	}
	for (b2Body *b : m_toSleet) {
		for (auto itr = m_snowParticles.begin(); itr != m_snowParticles.end(); itr++) {
			if (&*itr->get()->body == b) {
				auto p = itr->get()->getPosition();
				auto sleet = shared_ptr<ofxBox2dCircle>(new ofxBox2dCircle);
				sleet.get()->setPhysics(RAINDENSE+SNOWDENSE, SNOWBOUNCE, SNOWFRIC);
				sleet.get()->setup(m_world.getWorld(), p.x, p.y, 3);
				sleet.get()->body->SetGravityScale(1.0);
				sleet.get()->setData((void*)(SLEET));
				m_sleetParticles.push_back(sleet);
				m_snowParticles.erase(itr);
				break;
			}
		}
	}
	m_toSleet.clear();
	m_toDestroy.clear();
}

void ofApp::BeginContact(b2Contact *contact)
{
	b2Body *a = contact->GetFixtureA()->GetBody();
	b2Body *b = contact->GetFixtureB()->GetBody();
	if (!a || ! b) {
		return;
	}
	BODYTYPE at = (BODYTYPE) reinterpret_cast<int>(a->GetUserData());
	BODYTYPE bt = (BODYTYPE) reinterpret_cast<int>(b->GetUserData());
	if (at == bt) {
		return;
	}
	if (at == WIND) {
		m_toDestroy.insert(a);
	}
	if (bt == WIND) {
		m_toDestroy.insert(b);
	}
	if (at == RAIN) {
		if (bt == DEFAULT || bt == SNOW) {
			m_toDestroy.insert(a);
		}
		if (bt == SNOW) {
			m_toSleet.insert(b);
		}
	}
	if (bt == RAIN) {
		if (at == DEFAULT || at == SNOW) {
			m_toDestroy.insert(b);
		}
		if (at == SNOW) {
			m_toSleet.insert(a);
		}
	}
	if ((at == SLEET || at == SNOW) && bt == DEFAULT) {
		m_toDestroy.insert(a);
	}
	if ((bt == SLEET || bt == SNOW) && at == DEFAULT) {
		m_toDestroy.insert(b);
	}
}

void ofApp::setupMusic(bool forward)
{
	if (forward) {
		m_currTrack = (m_currTrack + 1) % m_tracks;
	}
	std::string name = m_dir.getName(m_currTrack);
	m_trackName = name.substr(0, name.find(".mp3"));
	m_player.load(m_dir.getAbsolutePath() + "\\" + name);
	m_player.play();
}

void ofApp::EndContact(b2Contact *contact)
{

}
