#pragma once

#include "ofMain.h"
#include "ofxBox2d.h"

class SaveState {
public:
	SaveState(float32 rot, b2Vec2 pos) : m_rot(rot), m_pos(pos) {}
	float32 m_rot;
	b2Vec2 m_pos;
};

enum BODYTYPE {
	DEFAULT=0,
	HOUSE=1,
	WIND=2,
	RAIN=3,
	SNOW=4,
	SLEET=5,
	STATIC=6
};

class ofApp : public ofBaseApp, b2ContactListener{

public:
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void mouseScrolled(int x, int y, float sx, float sy);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	void loadLevel(int level);
	void generateMessage(std::string name);
	void toggleSim();
	std::string getSimTimeMessage();
	void setCommonParams(float wind, float rain, float snow, float targetLevel, float targetTime, std::string name);
	void setupBuildBlock(shared_ptr<ofxBox2dBaseShape> shape);
	void setupStaticBlock(shared_ptr<ofxBox2dBaseShape> shape);
	void setupWind();
	void setupSnow();
	void setupRain();
	void setupUserData();
	void setupMusic(bool forward = true);
	void cleanup();
	bool validateState();
	bool validatePoint(const ofPoint &pt);
	~ofApp();
private:
	ofxBox2d m_world;
	ofSoundPlayer m_player;
	ofDirectory m_dir;
	float m_targetLevel;
	float m_rainRate, m_snowRate, m_windRate;
	float m_time, m_targetTime, m_eventTime;
	size_t m_tracks, m_currTrack;
	int m_level;
	bool m_playMusic;
	bool m_simulating;
	bool m_eventSuccess;
	std::string m_message, m_trackName;
	vector <shared_ptr<ofxBox2dBaseShape> > m_structure, m_levelStructure, m_windParticles, m_snowParticles, m_rainParticles, m_sleetParticles;
	vector <shared_ptr<ofxBox2dJoint> > m_joints;
	vector <SaveState> m_save;
	set <b2Body *> m_toDestroy, m_toSleet;
	set <int> m_beaten;
};
